﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerText : MonoBehaviour 
{
	[SerializeField]
	private Text clearedText;

	public void SetText(string text)
	{
		GetComponent<Text>().text = text;
	}

	public void AddLetter(string letter)
	{
		 clearedText.text += letter;
	}
}
