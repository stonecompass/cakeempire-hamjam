﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Customer : MonoBehaviour
{
	public event Action<int> OnMoneyChange;

	public bool IsCurrent { get; set; }

	[SerializeField]
	private Queue<KeyCode> keys;

	[SerializeField]
	private GameObject textPrefab;

	private bool lerpIn;
	private bool lerpOut;
	private bool movingUp;
	private float destinationY;

	private GameObject text;

	void Start()
	{
		var random = UnityEngine.Random.Range(0, 2);
		Debug.Log(random);

		keys = new Queue<KeyCode>();
		
		var textString = "";

		var keyCount = UnityEngine.Random.Range(3, 6);
		for(int i = 0; i < keyCount; i++)
		{
			var keyCode = Cakes.keys[UnityEngine.Random.Range(0, Cakes.keys.Length)];
			keys.Enqueue(keyCode);
			textString += keyCode.ToString();
		}

		text = Instantiate(textPrefab);
		text.transform.parent = GameObject.FindWithTag("UI").transform;
		text.GetComponent<CustomerText>().SetText(textString);
	}

	void Update()
	{
		if(lerpIn)
		{
			transform.position = Vector2.MoveTowards(transform.position, new Vector2(transform.position.x, destinationY), 2 * Time.deltaTime);

			if(Math.Abs(transform.position.y - destinationY) < 0.05f)
			{
				lerpIn = false;
				transform.position = new Vector2(transform.position.x, destinationY);
			}
		}
		else if(lerpOut)
		{
			transform.Translate(new Vector3(-4 * Time.deltaTime, 0));
		}
		else if(movingUp)
		{
			transform.position = Vector2.MoveTowards(transform.position, new Vector2(transform.position.x, destinationY), 2 * Time.deltaTime);

			if(Math.Abs(transform.position.y - destinationY) < 0.05f)
			{
				movingUp = false;
				transform.position = new Vector2(transform.position.x, destinationY);
			}
		}
		else
		{
			if(IsCurrent)
			{
				if(Input.GetKeyDown(keys.Peek()))
				{ 
					var key = keys.Dequeue();
					text.GetComponent<CustomerText>().AddLetter(key.ToString());
				}
				else if(Input.anyKeyDown)
				{
					FadeOut();
					OnMoneyChange(-50);
					IsCurrent = false;
				}

				if(keys.Count == 0)
				{
					FadeOut();
					OnMoneyChange(50);
					IsCurrent = false;
				}
			}
		}
		if(text != null)
			text.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(transform.position);	
	}

	public void FadeOut()
	{
		Destroy(text);
		lerpOut = true;
	}

	IEnumerator KillTimer()
	{
		yield return new WaitForSeconds(2f);
		Destroy(gameObject);
	}

	public void MoveUp()
	{
		movingUp = true;
		destinationY = transform.position.y + 0.3f;
	}

	public void LerpIn(float destinationY)
	{
		this.destinationY = destinationY;
		lerpIn = true;
	}
}