﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerManager : MonoBehaviour 
{
	[SerializeField]
	private int money;

	[SerializeField]
	private Text moneyText;

	[SerializeField]
	private Queue<Customer> customers;

	[SerializeField]
	private GameObject customerPrefab;

	[SerializeField]
	private Transform store0SpawnPoint;

	private Customer currentCustomer;

	void Start() 
	{
		customers = new Queue<Customer>();
		
		var customer = Instantiate(customerPrefab).transform;
		customers.Enqueue(customer.GetComponent<Customer>());

		customer.parent = transform;
		customer.transform.position = store0SpawnPoint.position;
		customer.GetComponent<Customer>().OnMoneyChange += OnMoneyChange;

		customer = Instantiate(customerPrefab).transform;
		customers.Enqueue(customer.GetComponent<Customer>());
		customer.parent = transform;
		customer.transform.position = new Vector3(store0SpawnPoint.position.x, store0SpawnPoint.position.y - 0.3f, store0SpawnPoint.position.z - 1);
		customer.GetComponent<Customer>().OnMoneyChange += OnMoneyChange;

		customer = Instantiate(customerPrefab).transform;
		customers.Enqueue(customer.GetComponent<Customer>());
		customer.parent = transform;
		customer.transform.position = new Vector3(store0SpawnPoint.position.x, store0SpawnPoint.position.y - 0.3f * 2, store0SpawnPoint.position.z - 1);
		customer.GetComponent<Customer>().OnMoneyChange += OnMoneyChange;

		customer = Instantiate(customerPrefab).transform;
		customers.Enqueue(customer.GetComponent<Customer>());
		customer.parent = transform;
		customer.transform.position = new Vector3(store0SpawnPoint.position.x, store0SpawnPoint.position.y - 0.3f * 3, store0SpawnPoint.position.z - 1);
		customer.GetComponent<Customer>().OnMoneyChange += OnMoneyChange;

		customer = Instantiate(customerPrefab).transform;
		customers.Enqueue(customer.GetComponent<Customer>());
		customer.parent = transform;
		customer.transform.position = new Vector3(store0SpawnPoint.position.x, store0SpawnPoint.position.y - 0.3f * 4, store0SpawnPoint.position.z - 1);
		customer.GetComponent<Customer>().OnMoneyChange += OnMoneyChange;

		customer = Instantiate(customerPrefab).transform;
		customers.Enqueue(customer.GetComponent<Customer>());
		customer.parent = transform;
		customer.transform.position = new Vector3(store0SpawnPoint.position.x, store0SpawnPoint.position.y - 0.3f * 5, store0SpawnPoint.position.z - 1);
		customer.GetComponent<Customer>().OnMoneyChange += OnMoneyChange;
		
		currentCustomer = customers.Dequeue();
		currentCustomer.IsCurrent = true;
	}
	
	void Update () 
	{
		moneyText.text = money + "";

		if(!currentCustomer.IsCurrent)
		{
			foreach(var customer in customers)
			{
				customer.MoveUp();
			}

			var c = Instantiate(customerPrefab).transform;
			customers.Enqueue(c.GetComponent<Customer>());
			c.parent = transform;
			c.transform.position = new Vector3(store0SpawnPoint.position.x, store0SpawnPoint.position.y - 2, store0SpawnPoint.position.z - 1);
			c.GetComponent<Customer>().LerpIn(store0SpawnPoint.position.y - 0.3f * 5);
			c.GetComponent<Customer>().OnMoneyChange += OnMoneyChange;
			
			if(customers.Count != 0)
			{
				currentCustomer = customers.Dequeue();
				currentCustomer.IsCurrent = true;
			}
			else
			{

			}
		}
	}

	protected void OnMoneyChange(int amount)
	{
		money += amount;
	}
}
